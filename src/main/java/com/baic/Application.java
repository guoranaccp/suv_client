package com.baic;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
@Configuration
//@MapperScan({"com.baic.*.mapper","org.example.*.mapper"})
@MapperScan("com.baic.project.system.user.dao")
public class Application {
@RequestMapping("hello")
@ResponseBody
public String hello(){
return"hello world！";
    }

public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
