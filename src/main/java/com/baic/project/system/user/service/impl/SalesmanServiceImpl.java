package com.baic.project.system.user.service.impl;

import com.baic.project.system.user.entity.Salesman;
import com.baic.project.system.user.dao.SalesmanMapper;
import com.baic.project.system.user.service.SalesmanService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Service
public class SalesmanServiceImpl extends ServiceImpl<SalesmanMapper, Salesman> implements SalesmanService {

}
