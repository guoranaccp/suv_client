package com.baic.project.system.user.service.impl;

import com.baic.project.system.user.entity.Code;
import com.baic.project.system.user.dao.CodeMapper;
import com.baic.project.system.user.service.CodeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Service
public class CodeServiceImpl extends ServiceImpl<CodeMapper, Code> implements CodeService {

}
