package com.baic.project.system.user.service;

import com.baic.project.system.user.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 原来的字段是4s现在改成了fourshop，因为数字的列名在java中报语法错误 服务类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface UserService extends IService<User> {
    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    User selectUserByName(String userName);
}
