package com.baic.project.system.user.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baic.project.system.user.dao.AdminMapper;
import com.baic.project.system.user.dao.UserMapper;
import com.baic.project.system.user.domain.SysMenu;
import com.baic.project.system.user.domain.SysUser;
import com.baic.project.system.user.entity.Admin;
import com.baic.project.system.user.entity.User;
import com.baic.system.project.tools.config.Global;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Controller
public class AdminController {
	@Autowired
    private UserMapper userMapper;
	@Autowired
	private AdminMapper adminMapper;
	
	//后台管理系统_页面跳转
    @GetMapping("/login")
    public String login()
    {
        return "login";
    }

    //后台管理系统_登录验证
    @PostMapping("/check")
    @ResponseBody
    public String checkLogin(@RequestParam("username") String username, @RequestParam("password") String password, Map<String,Object> map){
        Admin loginUser = adminMapper.selectAdminUserByName(username);
        String userPassword = loginUser.getPass();
        System.out.println(userPassword);
        if(userPassword.equals(password)){
            return "index";
        }else {
            map.put("msg","用户名密码错误");
            return "login";
        }
    }

    //后台管理系统_系统首页
    @GetMapping("/index")
    public String index(@RequestParam("username") String username,ModelMap mmap)
    {
    	System.out.println("登录的用户名："+username);
    	//取身份信息
    	User loginUser = userMapper.selectUserByName(username);
        SysUser user = new SysUser();
        // 根据用户id取出菜单
        //List<SysMenu> menus = menuService.selectMenusByUser(user);
        List<SysMenu> menus = new ArrayList<SysMenu>();
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
        //mmap.put("demoEnabled", Global.isDemoEnabled());
        mmap.put("demoEnabled", true);
        return "index";
    }
    
    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap)
    {
        mmap.put("version", Global.getVersion());
        return "main";
    }
    
    // 系统介绍1
    @GetMapping("/system/main_v1")
    public String main_v1(ModelMap mmap)
    {
        mmap.put("version", Global.getVersion());
        return "main_v1";
    }
    
    @GetMapping("/select")
    @ResponseBody
    public User selectUser(){
        // 查询 User
        User exampleUser = userMapper.selectById(1);
        return exampleUser;
    }
	
}

