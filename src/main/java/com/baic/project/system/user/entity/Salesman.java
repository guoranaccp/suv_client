package com.baic.project.system.user.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@TableName("my_salesman")
public class Salesman extends Model<Salesman> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String user;
    private String pass;
    private String name;
    private String phone;
    private String shop;
    private Integer point;
    private String ip;
    @TableField("login_time")
    private Integer loginTime;
    @TableField("reg_time")
    private Integer regTime;
    private String email;
    private String token;
    @TableField("token_exptime")
    private Integer tokenExptime;
    private Integer status;
    private String oid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Integer loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getRegTime() {
        return regTime;
    }

    public void setRegTime(Integer regTime) {
        this.regTime = regTime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getTokenExptime() {
        return tokenExptime;
    }

    public void setTokenExptime(Integer tokenExptime) {
        this.tokenExptime = tokenExptime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Salesman{" +
        "id=" + id +
        ", user=" + user +
        ", pass=" + pass +
        ", name=" + name +
        ", phone=" + phone +
        ", shop=" + shop +
        ", point=" + point +
        ", ip=" + ip +
        ", loginTime=" + loginTime +
        ", regTime=" + regTime +
        ", email=" + email +
        ", token=" + token +
        ", tokenExptime=" + tokenExptime +
        ", status=" + status +
        ", oid=" + oid +
        "}";
    }
}
