package com.baic.project.system.user.dao;

import com.baic.project.system.user.entity.Salesman;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface SalesmanMapper extends BaseMapper<Salesman> {

}
