package com.baic.project.system.user.service.impl;

import com.baic.project.system.user.entity.Admin;
import com.baic.project.system.user.entity.User;
import com.baic.project.system.user.dao.AdminMapper;
import com.baic.project.system.user.dao.UserMapper;
import com.baic.project.system.user.service.AdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {
	@Autowired
	private AdminMapper adminMapper;
	@Override
	public Admin selectAdminUserByName(String name) {
		// TODO Auto-generated method stub
		return adminMapper.selectAdminUserByName(name);
	}
}
