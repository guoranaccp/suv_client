package com.baic.project.system.user.service;

import com.baic.project.system.user.entity.Salesman;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface SalesmanService extends IService<Salesman> {

}
