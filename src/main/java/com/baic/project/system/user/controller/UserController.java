package com.baic.project.system.user.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.baic.project.system.user.dao.AdminMapper;
import com.baic.project.system.user.dao.UserMapper;
import com.baic.project.system.user.domain.SysUser;
import com.baic.project.system.user.entity.Admin;
import com.baic.project.system.user.entity.User;
import com.baic.system.project.tools.config.Global;
import com.baic.project.system.user.domain.*;
/**
 * <p>
 * 原来的字段是4s现在改成了fourshop，因为数字的列名在java中报语法错误 前端控制器
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Controller
public class UserController {
	
}

