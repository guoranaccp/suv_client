package com.baic.project.system.user.service;

import com.baic.project.system.user.entity.Admin;
import com.baic.project.system.user.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface AdminService extends IService<Admin> {
    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    Admin selectAdminUserByName(String name);
}
