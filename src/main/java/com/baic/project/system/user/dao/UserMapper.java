package com.baic.project.system.user.dao;

import org.apache.ibatis.annotations.Select;

import com.baic.project.system.user.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 原来的字段是4s现在改成了fourshop，因为数字的列名在java中报语法错误 Mapper 接口
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from sys_user where login_name = #{loginName}")
    User selectUserByName(String loginName);
}	