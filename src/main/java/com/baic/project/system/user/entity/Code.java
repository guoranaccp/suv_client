package com.baic.project.system.user.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@TableName("my_code")
public class Code extends Model<Code> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("saller_id")
    private Integer sallerId;
    @TableField("saller_name")
    private String sallerName;
    @TableField("saller_phone")
    private String sallerPhone;
    @TableField("user_id")
    private Integer userId;
    @TableField("user_name")
    private String userName;
    @TableField("user_phone")
    private String userPhone;
    @TableField("cat_id")
    private Integer catId;
    private String code;
    @TableField("create_time")
    private Integer createTime;
    @TableField("shop_id")
    private Integer shopId;
    private Integer stat;
    @TableField("use_time")
    private Integer useTime;
    @TableField("end_time")
    private Integer endTime;
    @TableField("car_id")
    private Integer carId;
    @TableField("car_name")
    private String carName;
    private Integer points;
    private String other;
    private String dingjin;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSallerId() {
        return sallerId;
    }

    public void setSallerId(Integer sallerId) {
        this.sallerId = sallerId;
    }

    public String getSallerName() {
        return sallerName;
    }

    public void setSallerName(String sallerName) {
        this.sallerName = sallerName;
    }

    public String getSallerPhone() {
        return sallerPhone;
    }

    public void setSallerPhone(String sallerPhone) {
        this.sallerPhone = sallerPhone;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getStat() {
        return stat;
    }

    public void setStat(Integer stat) {
        this.stat = stat;
    }

    public Integer getUseTime() {
        return useTime;
    }

    public void setUseTime(Integer useTime) {
        this.useTime = useTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getDingjin() {
        return dingjin;
    }

    public void setDingjin(String dingjin) {
        this.dingjin = dingjin;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Code{" +
        "id=" + id +
        ", sallerId=" + sallerId +
        ", sallerName=" + sallerName +
        ", sallerPhone=" + sallerPhone +
        ", userId=" + userId +
        ", userName=" + userName +
        ", userPhone=" + userPhone +
        ", catId=" + catId +
        ", code=" + code +
        ", createTime=" + createTime +
        ", shopId=" + shopId +
        ", stat=" + stat +
        ", useTime=" + useTime +
        ", endTime=" + endTime +
        ", carId=" + carId +
        ", carName=" + carName +
        ", points=" + points +
        ", other=" + other +
        ", dingjin=" + dingjin +
        "}";
    }
}
