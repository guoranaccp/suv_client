package com.baic.project.system.user.service.impl;

import com.baic.project.system.user.entity.User;
import com.baic.project.system.user.dao.UserMapper;
import com.baic.project.system.user.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 原来的字段是4s现在改成了fourshop，因为数字的列名在java中报语法错误 服务实现类
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
	@Autowired
    private UserMapper userMapper;
	@Override
    public User selectUserByName(String userName) {
        return userMapper.selectUserByName(userName);
    }
}
