package com.baic.project.system.user.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 原来的字段是4s现在改成了fourshop，因为数字的列名在java中报语法错误
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
@TableName("my_user")
public class User extends Model<User> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private String user;
    private String pass;
    private String phone;
    /**
     * 成交数量
     */
    private Integer num;
    @TableField("parent_id")
    private Integer parentId;
    private String name;
    @TableField("success_time")
    private Integer successTime;
    private String ip;
    @TableField("login_time")
    private Integer loginTime;
    @TableField("reg_time")
    private Integer regTime;
    @TableField("code_id")
    private String codeId;
    @TableField("code_time")
    private Integer codeTime;
    private String fourshop;
    private String car;
    private String diqu;
    private String sfzid;
    /**
     * 付款信息-全款-贷款:北汽财务-贷款:其他
     */
    private String zhifu;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(Integer successTime) {
        this.successTime = successTime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Integer loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getRegTime() {
        return regTime;
    }

    public void setRegTime(Integer regTime) {
        this.regTime = regTime;
    }

    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    public Integer getCodeTime() {
        return codeTime;
    }

    public void setCodeTime(Integer codeTime) {
        this.codeTime = codeTime;
    }

    public String getFourshop() {
        return fourshop;
    }

    public void setFourshop(String fourshop) {
        this.fourshop = fourshop;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getDiqu() {
        return diqu;
    }

    public void setDiqu(String diqu) {
        this.diqu = diqu;
    }

    public String getSfzid() {
        return sfzid;
    }

    public void setSfzid(String sfzid) {
        this.sfzid = sfzid;
    }

    public String getZhifu() {
        return zhifu;
    }

    public void setZhifu(String zhifu) {
        this.zhifu = zhifu;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
        "id=" + id +
        ", user=" + user +
        ", pass=" + pass +
        ", phone=" + phone +
        ", num=" + num +
        ", parentId=" + parentId +
        ", name=" + name +
        ", successTime=" + successTime +
        ", ip=" + ip +
        ", loginTime=" + loginTime +
        ", regTime=" + regTime +
        ", codeId=" + codeId +
        ", codeTime=" + codeTime +
        ", fourshop=" + fourshop +
        ", car=" + car +
        ", diqu=" + diqu +
        ", sfzid=" + sfzid +
        ", zhifu=" + zhifu +
        "}";
    }
}
