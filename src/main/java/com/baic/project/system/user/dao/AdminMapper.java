package com.baic.project.system.user.dao;

import org.apache.ibatis.annotations.Select;

import com.baic.project.system.user.entity.Admin;
import com.baic.project.system.user.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author GuoRan
 * @since 2019-11-18
 */
public interface AdminMapper extends BaseMapper<Admin> {
	
    @Select("select * from my_admin where name = #{name}")
    Admin selectAdminUserByName(String name);
}	
